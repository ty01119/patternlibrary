$(document).ready(function() {
    $(document).mousemove(function(event) {
        $('#instructions').hide();
        
        cx = Math.ceil($(window).width() / 2.0);
        cy = Math.ceil($(window).height() / 2.0);
        dx = event.pageX - cx;
        dy = event.pageY - cy;
        
        console.log(cx,cy,dx,dy);
        
        tiltx = (dy / cy);
        tilty = (dx / cx);
        radius = Math.sqrt(Math.pow(tiltx,2) + Math.pow(tilty,2));
        degree = (radius * 20);
        
        // 
        // 
        // x0 = Math.ceil(cx - (dx * 0.2));
        // y0 = Math.ceil(cy - (dy * 0.2));
        // x1 = event.pageX;
        // y1 = event.pageY;
        // r0 = 300;
        // r1 = 10;
        // 
        // sx = -dx * 0.03;
        // sy = -dy * 0.03;
        // b  =  (Math.abs(sx) + Math.abs(sy)) * 0.2;

        $('#picture').css('-webkit-transform','rotate3d(' + -tiltx + ', ' + tilty + ', 0, ' + degree*0.5 + 'deg) translate3d('+ -tiltx*2 +'%, '+ tilty*2 +'%, 0)');
        $('#picture').css('transform','rotate3d(' + -tiltx + ', ' + tilty + ', 0, ' + degree*0.5 + 'deg) translate3d('+ -tiltx*2 +'%, '+ tilty*2 +'%, 0)');
        $('.layer3').css('-webkit-transform','rotate3d(' + -tiltx + ', ' + tilty + ', 0, ' + degree*1.5 + 'deg) translate3d('+ -tiltx*5 +'%, '+ tilty*5 +'%, 0)');
        $('.layer3').css('transform','rotate3d(' + -tiltx + ', ' + tilty + ', 0, ' + degree*1.5 + 'deg) translate3d('+ -tiltx*5 +'%, '+ tilty*5 +'%, 0)');
        
        // $('#picture').css('-webkit-transform','rotateY(20deg)');
        
        // $('body').css('background-image', "-webkit-gradient(radial, " + x0 +" " + y0 +", "+ r0 +", "+ x1 +" "+ y1 +", "+ r1 +", from(#575757), to(#FFFFFF))");
        // $('h1').css('text-shadow', "#444 "+ sx +"px "+ sy + "px " + b + "px");
    });
});