<!DOCTYPE html>
<html>
<head>
	<title>Parallax Pattern</title>
</head>
<body style="margin: 0;">
<div id="tilt">
	<div style="position: absolute; -webkit-perspective: 1000; -moz-perspective: 1000px; transform-style: preserve-3d; width: 100%; height: 100%; overflow: hidden; background-color: #0f111d;" >
          <div id="picture" style="height: 100%; width: 100%; -webkit-transform-style: preserve-3d; transform-style: preserve-3d; background: url('./assets/images/bg.jpg') no-repeat; background-size: 100% 100%;">
                  
          </div>
          <div class="layer3" style="width: 100%; height: 100%; top: 0; left: 0; position: absolute; -webkit-transform: translateZ(20px); background-position: right; -moz-transform: translateZ(20px);">
            <div style="width: 70%; height: 70%; background: url('./assets/images/fg1-bw.png') no-repeat;position: absolute; top: 15%; right: 2%; background-size: contain; overflow: initial;">  
            </div>
            <div style="position: absolute; top: 20%; left: 20%; background: rgba(0,0,0,0.6); font-family: 'Lobster', arial, serif; color: #FFF; font-size: 30px; line-height: 60px; text-align:center; overflow: initial;"> 
              Demo 1: Camera Tilt Effect
            </div>
          </div>
        </div>
</div>
</body>
<script src="./assets/plugins/jquery/jquery-3.2.1.min.js"></script>
<script src="./assets/javascript/script.js"></script>
</html>